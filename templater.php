<?php

/**
 * Easily Manage Plugin & Theme Page templates for Wordpress
 * 
 * @author  Marley Plant <marley@marleyplant.com>
 *
 * @since 1.0
 *
 * @param string $templateFolder  Path to templates for the engine.
 */

class PlantekTemplater
{
    var $archiveTemplates = [];
    var $singleTemplates = [];
    var $taxonomyTemplates = [];
    var $templateFolder = '';

    function __construct($templateFolder)
    {
        if (!file_exists($templateFolder)) {
            // Display Error Message
            return false;
        }

        $this->templateFolder = $templateFolder;
        add_action('archive_template', [$this, 'archiveHook']);
        add_action('single_template', [$this, 'singleHook']);
        add_action('taxonomy_template', [$this, 'taxonomyHook']);
    }

    /**
     * Generates an object for thr array containing template information
     * 
     * @param string $posttype
     * @param strint $type
     * @return array
     */
    function generateObject($type, $posttype)
    {
        $path = $this->templateFolder . '/' . $type . '-' . $posttype . '.php';
        return [
            "posttype" => $posttype,
            "path" => $path
        ];
    }

    /**
     * Automatically Add Templates From the Template Directory using scandir()
     */
    function autoInclude()
    {
        if ($this->templateFolder == '') return false;

        $templates = scandir($this->templateFolder);

        foreach ($templates as $template) {
            //Split filename into post type and archivr type
            $templatetype = 'archive';
            $posttype = 'post';

            addTemplate($templatetype, $posttype);
        }
    }


    /**
     * Add a template to the engine
     * 
     * @param string $posttype
     * @param strint $type
     * @return bool
     */
    function addTemplate($type, $posttype = 'post')
    {
        if ($type == 'single') {
            array_push($this->singleTemplates, $this->generateObject($type, $posttype));
        } elseif ($type == 'archive') {
            array_push($this->archiveTemplates, $this->generateObject($type, $posttype));
        } elseif ($type == 'taxonomy') {
            array_push($this->taxonomyTemplates, $this->generateObject($type, $posttype));
        }
    }

    /**
     * Display the correct template from the engine
     * this function can only be implemented using the wordpress single_template hook
     * 
     * @param string $single
     * @return string
     */
    function singleHook($single)
    {
        global $post;
        foreach ($this->singleTemplates as $template) {
            if (!file_exists($template['path'])) continue;
            if (!$post->post_type == $template['posttype']) continue;
            return $template['path'];
        }

        return $single;
    }

    function taxonomyHook()
    {
        if( is_tax('broadcaster') ) {
            
            foreach ($this->taxonomyTemplates as $template) {
                if (!file_exists($template['path'])) continue;
    
                $tax_tpl = $template['path'];
            }

            include $tax_tpl;
            die();
        }
    }


    /**
     * Display the correct template from the engine
     * this function can only be implemented using the wordpress archive_template hook
     * 
     * @param string $archive
     * @return string
     */
    function archiveHook($archive)
    {
        global $post;

        foreach ($this->archiveTemplates as $template) {
            if (!file_exists($template['path'])) continue;
            if (!$post->post_type == $template['posttype']) continue;
            return $template['path'];
        }

        return $archive;
    }
}
